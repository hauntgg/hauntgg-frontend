import * as common from "./webpackcommon";
import * as mm from "./src/metamask";

// one of these will be null
export {
  common,
  mm
  // polyfill,
  // wasm
}
