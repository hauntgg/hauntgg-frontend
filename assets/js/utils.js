const list_sel = () => document.querySelector("#list");

let humanize_num = (x) => {
  return x > 10000 ? (((x / 1000) * 100) / 100).toFixed(2) + "K" : x;
}

let invert_list = (list) => {
  // list should be an interatble element with .children property
  if (list)
    [...list.children].reverse().map(node => list.appendChild(node));

}

// checks for metamask, calls function if successful
let mm_check = async (cb) => {
  let connect = document.querySelector("#web3connect");
  try {

    let dapp = await window.EntryPoint.mm.detectEthereumProvider();
    if (dapp) {
      if (await window.EntryPoint.mm.on_metamask_matic_network()) {
        connect.innerText = "Metamask + Matic found! Loading live data...";
        cb(); // if on matic, run the setup func
      }
    } else {
      throw dapp;
    }

  } catch (e) {
    if (connect) {
      connect.innerText = "😞 Metamask / Matic not found, try a snapshot?";
      connect.href = "./snapshot/";
      connect.classList.remove("animate-pulse");
      connect.classList.add("border");
    } else console.log("tried to mm check, could find the web3 connect btn");
  }
};

// from old thing
let rarity_filter = (e) => {

  console.log("rarity_filter", e);

  let list = list_sel();


  [...list.children].map(x => {

    if (x.dataset.rarity.toLowerCase() === e.toLowerCase() || e.toLowerCase() === "all")
      x.classList.remove("hide")
    else x.classList.add("hide")

  });

  // if (e.toLowerCase() === "all") {
  //   // unhide all
  //   [...list.children].map(x => {
  //     x.classList.remove("hide")
  //   });
  // }
  //
  // else {
  //   // xp only
  //   [...list.children].map(x => {
  //     if (x.dataset.rarity.toLowerCase() !== e.toLowerCase())
  //     x.classList.add("hide")
  //     else x.classList.remove("hide")
  //   })
  // }

};

// takes a dataset tag to try and sort the listings by
// limited to numeric sorting i believe
let smart_sort = (sortby) => {

  console.log("smart_sort this list by: ", sortby);

  let list = list_sel();

  let nodes = [...list.children];

  if (nodes)
    nodes.sort((a, b) => parseInt(b.dataset[sortby]) - parseInt(a.dataset[sortby]));


  // by default we would sort high index on top because newer
  if (sortby === "price" || sortby === "number") // same for "number" in portal section
    nodes.reverse(); // we want to sort low by default with price

  let invert = document.querySelector("#invert").checked;
  if (invert) nodes.reverse()

  // i could cancel out the reverse

  nodes.map(node => list.appendChild(node));


  // let d = document.querySelector("select#sort").selectedIndex;
  //
  // // latest listing
  // if (d == 0) {
  //
  //   let sort = [...list.children]
  //     .sort((a, b) => parseInt(b.dataset.index) - parseInt(a.dataset.index));
  //
  //   // if (invert) sort = sort.reverse();
  //   sort.forEach(node => list.appendChild(node));
  // }
  //
  // // price
  // if (d == 1) {
  //   let sort = [...list.children]
  //     .sort((a, b) => parseInt(a.dataset.price) - parseInt(b.dataset.price))
  //
  //   // if (invert) sort = sort.reverse();
  //   sort.forEach(node => list.appendChild(node));
  // }

  // @todo: maybe bring this before the above and reverse before sorting
  // let invert = document.querySelector("#invert").checked;
  // if (invert) invert_list(list);

  // else if (d == 1)
  // [...list.children]
  // .sort((a,b)=>parseInt(a.dataset.index) - parseInt(b.dataset.index))
  // .forEach(node=>list.appendChild(node));
}

let search_filter = (e) => {
  console.log("Search filter", e);

  let list = list_sel();

  [...list.children].map(x => {
    if (e.length > 0 && !x.dataset.name.toLowerCase().includes(e.toLowerCase()))
      x.classList.add("notsearched")
    else x.classList.remove("notsearched")
  });

  let select_rarity = document.querySelector("select#filter").value;
  // if (select_rarity.toLowerCase() !== "all")
  rarity_filter(select_rarity)

}
